extern printf
global resolvente

section .data
varA dq 0
varB dq 0
varC dq 0
result db "resultado:", 10,13,0
raiz1 db "raiz 1: %f",10,13,0
raiz2 db "raiz 2: %f",10,13,0
cuatro dq 4.0
dos dq 2.0

section .bss
buffer1: resq 1
buffer2: resq 1
buffer3: resq 1
buffer4: resq 1
buffer5: resq 1

section .text
resolvente:
    push ebp
    mov ebp, esp
    
    fld dword[ebp+8]
    fst qword[varA]
    fld dword[ebp+12]
    fst qword[varB]
    fld dword[ebp+16]
    fst qword[varC]
    
    mov esp, ebp
    pop ebp
    
    fld qword[varB]
    fld qword[varB]
    fmul
    fstp qword[buffer1]
    ;b2
    
    fld qword[cuatro]
    fld qword[varA]
    fmul
    fld qword[varC]
    fmul
    fstp qword[buffer2]
    ;4.a.c
    
    fld qword[buffer1]
    fld qword[buffer2]
    fsub
    fstp qword[buffer3]
    ;resta
    
    fld qword[buffer3]
    fsqrt
    fstp qword[buffer3]
    ;raiz
    
    fld qword[dos]
    fld qword[varB]
    fchs
    fld qword[buffer3]
    fsub
    fld qword[varA]
    fmul st0, st2
    fdiv
    fst qword[buffer4]
    ;-
    
    fld qword[dos]
    fld qword[varB]
    fchs
    fld qword[buffer3]
    fadd
    fld qword[varA]
    fmul st0, st2
    fdiv
    fst qword[buffer5]
    ;+
    
    push result
    call printf
    add esp, 4
    
    push dword[buffer4+4]
    push dword[buffer4]
    push raiz1
    call printf
    add esp, 12

    push dword[buffer5+4]
    push dword[buffer5]
    push raiz2
    call printf
    add esp, 12
    
    xor eax, eax
    ret
