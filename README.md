# Organización del computador 2 - Trabajo Practico número 1



## Formula Resolvente
Consigna: 

- Realizar un programa para la arquitectura IA32 que calcule las raíces de una función
cuadrática a través de la fórmula resolvente, cuyos valores pueden o no ser de punto flotante.

- Hacer un programa en C, que solicite al usuario los valores a, b, c; e invoque a la
función resolvente.

- Compilar y linkear los archivos objeto de manera separada. Obtener un ejecutable que
muestre por consola las raíces obtenidas.

Consideraciones:

-𝑏^2 − 4𝑎𝑐 ≥ 0, ∀ 𝑎, 𝑏, 𝑐 ϵ 𝑅

𝑎 > 0, 𝑎 ϵ 𝑅

Ejecucion:

Dentro de la carpeta del trabajo tenemos tres archivos. _Resolvente.asm_ donde se encuentra el codigo de la formula, _main.c_ donde se reciben los parametros y se llama la funcion , y finalmente _link.sh_ donde se encuentran los comandos para compilar y linkear los archivos.

![resolvente1](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/resolvente__1_.png)

Corremos el archivo link.sh y se ejecutarán dos comandos:

![resolvente3](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/resolvente__3_.png)

El primero compila el codigo assembler en un archivo objeto, y el segundo linkea el codigo en _c_ con el _.obj_ recien creado, resultando en un archivo _ejecutable_.

![resolvente2](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/resolvente__2_.png)

Corremos el _ejecutable_ y nos pedira ingresar valores para las variables a, b y c

![resolvente4](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/resolvente__4_.png)

Una vez ingresados los valores, el programa calculara las raices de la funcion y nos las mostrara por consola

![resolvente5](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/resolvente__5_.png)

## Producto Escalar
Consigna:

- Escribir una función en assembler IA-32 que reciba un número r y un puntero a un
vector de números de punto flotante, que calcule el producto escalar.

Ejecucion:

Dentro de la carpeta del trabajo tenemos tres archivos de la misma forma que en punto anterior. Corremos el archivo _link.sh_ y obtenemos un _.obj_ y el archivo ejecutable

![prod1](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/prod1.png)

Ejecutamos el archivo exec y nos pedira ingresar un numero

![prod2](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/prod2.png)

En esta funcion, tenemos un vector de numeros de punto flotante definido de esta forma

![vector1](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/vector1.png)

y sera modificado por una funcion en la cual se multiplicara cada uno de sus elementos por el numero ingresados

![vector2](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/vector2.png)

El vector resultante se muestra por consola

![prod3](https://gitlab.com/agustinmacias/orga2-tp1/-/raw/main/imagenes/prod3.png)

*Si bien los resultados se muestran correctamente por consola, al finalizar la operacion marca un error que no se pudo resolver.


