extern printf
global prodEscalar

section .data
vector dq 9.3,3.5,5.0,14.0,6.19,0.0
numero dq 0
msg db "%f, %f, %f, %f, %f",10,13,0

section .text
prodEscalar:

    push ebp
    mov ebp, esp
    fld dword[ebp+8]
    fst qword[numero]
    mov esp, ebp
    pop ebp

    mov ebp, esp;
    xor eax, eax
    mov ebx, vector
    mov edx, 0
loop:
    ffree
    fldz
    fld qword[ebx+edx*8]
    fcomi
    jz print
    fld qword[numero]
    fmulp
    fstp qword[ebx+edx*8]
    inc edx
    jmp loop
print:
    push dword[vector+4]
    push dword[vector]
    push dword[vector+12]
    push dword[vector+8]
    push dword[vector+20]
    push dword[vector+16]
    push dword[vector+28]
    push dword[vector+24]
    push dword[vector+36]
    push dword[vector+32]
    push msg
    call printf
    add esp, 44
    
    xor eax, eax
    ret
